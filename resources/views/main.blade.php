<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>막사지막!</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vivify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</head>
<body>
    <nav>
        <div class="come_on">
            <ul>
                <li><span class="pointer" id="login"><a href="/manage">로그인</a></span></li>
                <li><a href="/signup"><span>회원가입</span></a></li>
            </ul>
        </div>
    </nav>
    <header>
        <div class="container">
            <div class="text">
                <h3 class="show_text">소개임, 근데 짧으면 뻘쭘해서 좀 길게 ㅇㅇ</h3>
            </div>
        </div>
    </header>

    <div class="login-win vivify fadeIn duration-500">
        <div class="container">
            <div class="close">
                <i id="close" class="fa fa-close"></i>
            </div>
            <div class="form-group">
                <form action="" method="post">
                    <div class="input-group">
                        <label for="user_id">아이디</label>
                        <input type="text" name="user_id" id="user_id" placeholder="아이디를 입력하세요">
                    </div>
                    <div class="input-group">
                        <label for="password">비밀번호</label>
                        <input type="password" name="password" id="password" placeholder="비밀번호를 입력하세요">
                    </div>
                    <div class="input-group">
                        <input type="submit" value="로그인">
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
</html>