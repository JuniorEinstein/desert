@extends('layout')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">새로운 상품 등록</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="">
                        {{ csrf_field() }}
                        <div class="form-group">
                            코드 <input type="text" name="code" class="form-control">
                        </div>
                        <div class="form-group">
                            이름 <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            가격 <input type="text" name="price" class="form-control">
                        </div>
                        <div class="form-group">
                            포인트 <input type="text" name="point" class="form-control">
                        </div>
                        <div class="form-group">
                            종류 <input type="text" name="type" class="form-control">
                        </div>
                        <input type="submit" class="btn btn-primary" value="저장">
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
@endsection