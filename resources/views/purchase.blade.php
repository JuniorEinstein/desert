<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>막사지막!</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vivify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/purchase.css') }}">

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/purchase.js') }}"></script>
</head>
<body>
<div class="main-container">
    <nav class="group">
        <div class="container">

        </div>
    </nav>
    <div class="enroll-group">
        <div class="input-group group">
            <label for="membership">카드 등록</label>
            <br>
            <form>
                <input type="text" name="code" id="membership" placeholder="멤버십 먼가를 입력해주세요">
                <input type="button" value="등록">
            </form>
        </div>
        <div class="input-group group">
            <label for="product">상품 등록</label>
            <br>

            <input type="text" name="product" id="product" placeholder="상품 먼가를 입력해주세요">
            <input type="button" value="등록">

        </div>
    </div>
    <div class="main">
        <div class="info-group group">
            <div class="light-info inline group">
                <div class="card-img in">
                    <img src="{{ asset('img/card.png') }}" alt="card" class="card">
                </div>
                <div class="info in">
                    <ul>
                        <li class="user-head">사용자 정보</li>
                        <li>사용자 이름 : {{ $member->username }}</li>
                        <li>포인트 : {{ $member->point }}</li>
                    </ul>
                </div>
            </div>
            <div class="accumulation inline group">

            </div>
        </div>
        <div class="info-group group">
            <div class="item-info">
                <ul class="items">
                    <div class="item">
                        <li>코드</li>
                        <li>이름</li>
                        <li>가격</li>
                        <li>타입</li>
                        <li>마일리지 점수</li>
                    </div>
                    <form method="post" action="/buy/products">
                        {{ csrf_field() }}
                        <input type="hidden" name="member_id" value="{{ $member->id }}">
                        @foreach($products as $product)
                            @php
                            $code = $product->code;
                            $product = \App\Product::where('code', $code)->get()[0];
                            @endphp
                        <input type="hidden" name="products" value="{{ $product->code }}">
                            <div class="item">
                                <li>{{ $product->code }}</li>
                                <li>{{ $product->name }}</li>
                                <li>{{ $product->price }}</li>
                                <li>{{ $product->type }}</li>
                                <li>{{ $product->point }}</li>
                            </div>
                        @endforeach
                            <input type="submit" value="구매">
                    </form>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>