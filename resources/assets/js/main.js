$(document).ready(function(){
    $('#login').on('click',function(){
        $('.login-win').css('display','block');
        $('header').css('filter','brightness(43%)');
    });
    $('#close').on('click',function(){
        $('.login-win').css('display','none');
        $('header').css('filter','none');
    });

    // 중앙정렬
    var height = $('.show_text').height()/2;
    $('.text').css('top','calc(50% - '+height+'px)');
});

$(window).resize(function(){
    // 중앙정렬
    var height = $('.show_text').height()/2;
    $('.text').css('top','calc(50% - '+height+'px)');
});