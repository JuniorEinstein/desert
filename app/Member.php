<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = ['username', 'password', 'code'];

    public function datas()
    {
        return $this->hasMany('App\Data');
    }
}
