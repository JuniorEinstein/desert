<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    public function input(Request $request)
    {
        $member_id = $request->input('member_id');
        $code = $request->input('code');

        \App\Data::create([
            'member_id' => $member_id,
            'code' => $code
        ]);

        return "member_id : ".$member_id."<br>code : ".$code;
    }

    public function index(Request $request)
    {
        $id = $request->input('id');
        $id = $id? 1:\App\Member::get()[0]->id;

        $sales = \App\Data::get()->count();
        $members = \App\Member::get()->count();
        $point = \App\Member::find($id)->point;

        return view('manage', [
            'id' => $id,
            'sales' => $sales,
            'members' => $members,
            'point' => $point
        ]);
    }

    public function table()
    {
        $data = \App\Data::get();

        return view('table', [
            'datas' => $data
        ]);
    }
}
