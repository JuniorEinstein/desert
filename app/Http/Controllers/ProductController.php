<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = \App\Product::get();

        return view('product', [
            'products' => $products
        ]);
    }

    public function store(Request $request)
    {
        $code = $request->input('code');

        if (!empty($code)) {
            $member = \App\Member::where('code', $code)->get()[0];
            $products = \App\Data::where('done', 0)->get();
            return view('purchase', [
                'member' => $member,
                'products' => $products
            ]);
        } else {
            return view('purchase', [
                'username' => "",
                'point' => 0
            ]);
        }
    }

    public function add_product(Request $request)
    {
        $code = $request->input('code');
        $name = $request->input('name');
        $price = $request->input('price');
        $type = $request->input('type');

        \App\Product::create([
            'code' => $code,
            'name' => $name,
            'price' => $price,
            'type' => $type
        ]);

        return redirect('/manage/product');
    }

    public function add_product_page()
    {
        return view('product/add');
    }

    public function delete_product(Request $request)
    {
        $id = $request->input('id');

        \App\Product::find($id)->delete();

        return redirect('/manage/product');
    }

    public function buy(Request $request)
    {
        $member_id = $request->input('member_id');
        $products = $request->input('products');

        foreach ($products as $product) {

        }
    }

    public function buy_products(Request $request)
    {
        $member_id = $_POST['member_id'];
        $products = $_POST['products'];


        $item = \App\Product::where('code', $products)->get()[0];
        $point = \App\Member::where('id', $member_id)->get()[0]->point;
        $member = \App\Member::where('id', $member_id)->update(['point' => $point + $item->point]);


        \App\Data::where('code', $products)->update(['done' => 1]);


        return redirect('/manage/store?code=s0554');
    }
}
