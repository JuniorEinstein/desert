<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemberController extends Controller
{
    public function index()
    {

    }

    public function member(Request $request)
    {
        $member_id = $request->member_id;

        $point = \App\Member::find($member_id)->point;

        return $point;
    }
}
