<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $fillable = ['member_id', 'code'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
