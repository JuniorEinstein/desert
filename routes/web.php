<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
});

Route::get('/signup',function(){
    return view('signup');
});

Route::get('purchase',function(){
    return view('purchase');
});

Route::get('/member', 'MemberController@member');

Route::get('/data/input', 'DataController@input');
Route::get('/manage', 'DataController@index');
Route::get('/manage/table', 'DataController@table');

Route::get('/manage/product', 'ProductController@index');
Route::get('/manage/store', 'ProductController@store');
Route::get('/product/add_product', 'ProductController@add_product_page');
Route::post('/product/add_product', 'ProductController@add_product');
Route::get('/product/delete_product', 'ProductController@delete_product');

Route::get('/buy', 'ProductController@buy');
Route::post('/buy/products', 'ProductController@buy_products');